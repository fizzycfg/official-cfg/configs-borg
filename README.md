# Borg Configuration

Borg Configuration (fizzy-compliant).

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-borg/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
